# FOUILLE BBS

## Contexte Scientifique 

Les gliomes sont les tumeurs primitives du cerveau les plus courantes. 
Ils peuvent être classés en LGG (Lower-Grade Glioma) ou GBM (Glioblastoma Multiforme) en fonction des critères histologiques/d'imagerie. 
Les facteurs cliniques et moléculaires/de mutation sont également très cruciaux pour le processus de notation. 
Les tests moléculaires sont coûteux pour aider à diagnostiquer avec précision les patients atteints de gliome.
Dans cet ensemble de données, les 20 gènes et 3 caractéristiques cliniques les plus fréquemment mutés sont pris en compte dans les projets TCGA-LGG et TCGA-GBM sur les gliomes cérébraux. 
La tâche de prédiction consiste à déterminer si un patient est LGG ou GBM avec des caractéristiques cliniques et moléculaires/mutations données. 

L'objectif principal est de trouver le sous-ensemble optimal de gènes de mutation et de caractéristiques cliniques pour le processus de classification des gliomes afin d'améliorer les performances et de réduire les coûts.


https://archive.ics.uci.edu/dataset/759/glioma+grading+clinical+and+mutation+features+dataset
